import mongoose from "mongoose";

const userSchema = new mongoose.Schema(
    {
        userName : {
            type :String,
            required : true,
        },
        email : {
            type : String,
            required : true
        },
        address: [
            {
                detail :{
                    type: String
                },
                for :{
                    type : String
                }
            }
        ],
        phoneNo :{
            type:Number,
            required: true
        },
        
    }
);


export const userModel = mongoose.Model("users",userSchema);