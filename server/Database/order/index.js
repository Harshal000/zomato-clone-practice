import mongoose from "mongoose";

const OrderSchema = new mongoose.Schema({
    user : {
        type : mongoose.Types.ObjectId,
        ref : "users"
    },
    orderDetails :[{
        food : {
            type :mongoose.Types.ObjectId,
            ref : "foods"
        },
        quantity : {
            type :Number ,
            required : true
        },
        paymode : {
            type : String,
            required :true
        },
        status : {
            types : String,
            required : true
        },
        paymentDetail : {
            totalItem : {
                type : Number,
                required : true
            },
            promo : {
                type : Number,
                required : true
            },
            tax : {
                type : Number,
                required : true
            }
        }
    }],
    orderRating :{
        type : Number,
        required: true
    }
});

export const OrderModel = mongoose.Model("orders",OrderSchema);