import mongoose from "mongoose";

const reviewSchema = new mongoose.Schema({
  
    user : {
        type : mongoose.Types.ObjectId,
        ref : "users"
    },
    food : {
        type : mongoose.Types.ObjectId,
        ref : "foods"
    },
    restaurent : {
        type : mongoose.Types.ObjectId,
        ref : "restaurents"
    },
    rating: {
        type : Number,
        required: true
    },
    reviewText : {
        type : String,
        required : true
    },
    photos : [
        {
            type : mongoose.Types.ObjectId,
            ref : "images"
        }
    ]

});

export const reviewModel = mongoose.Model("reviews",reviewSchema);