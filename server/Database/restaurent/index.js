import mongoose from "mongoose";

const restaurentSchema = new mongoose.Schema({
    name :{
        type :String ,
        required :true
    },
    city : {
        type : String,
        required : true
    },
    address : {
        type : String ,
        required : true 
    },
    mapLocation : {
        type : String ,
        required : true
    },
    cuisine : [String],
    restaurentTime : String,
    contactNumber: Number,
    website : Number,
    popularDishes : [String],
    averageCost: Number,
    amenties : [String],
    menuImage : {
        type : mongoose.Types.ObjectId,
        ref : "images"
    },
    menus :{
        type :mongoose.Types.ObjectId,
        ref : "menus"
    },
    reviews : [
        {
            type :mongoose.Types.ObjectId,
            ref :"reviews"
        }
    ],
    photos : {
        type : mongoose.Types.Object,
        ref : "images"
    }


});

export const restaurentModel = mongoose.Model("restaurents",restaurentSchema);