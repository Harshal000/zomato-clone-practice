import mongoose from "mongoose";

const foodSchema = new mongoose.Schema({
      foodName : {
          type : String,
          required : true
      },
      description :{
          type : String,
          required : true
      },
      catrgory :{
          type : String,
          required : true
      },
      isVeg : {
          type :Boolean,
          required : true

      },
      isContainEgg :{
          type : Boolean,
          required : true 
      },
      price : {
          type :String ,
          default : 100,
          required : true
      },
      photo :{
          type : mongoose.Types.ObjectId,
          ref : "images"
      },
      addOns : [
        {
            type : mongoose.Types.ObjectId,
            ref : "foods"
        }
      ],
      restaurent : {
          type : mongoose.Types.ObjectId,
          ref : "restaurents",
          required : true
      }

});

export const foodModel = mongoose.Model('foods',foodSchema);