import { userModel } from "./user/index";
import { imageModel }   from "./image/index";
import { foodModel } from "./food/index";
import { restaurentModel } from "./restaurent/index";
import { menuModel } from "./menu/index";
import { reviewModel } from "./review/index";

export{
    userModel,imageModel,foodModel,restaurentModel,menuModel,reviewModel
};