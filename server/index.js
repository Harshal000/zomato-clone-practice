import express from "express";
import helmet from "helmet";
import cors from "cors";

const zomato = express();


zomato.use(express.json());
zomato.use(express.urlencoded({extended:false}));
zomato.use(cors());
zomato.use(helmet());

zomato.get("/",(req,res)=>{
    res.json("server start");
})

zomato.listen(4000,()=>console.log("server is running 🔥"));